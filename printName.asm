%macro read 2
mov eax, 3
mov ebx, 0
mov ecx, %1
mov edx, %2
int 0x80
ret
%endmacro

%macro print 2
  mov eax, 4
  mov ebx, 1
  mov ecx, %1
  mov edx, %2
  int 0x80
  ret
%endmacro

section .data
MSG1 db "What is your name?", 0xA, 0xD, 0
1Len equ $ - MSG1

MSG2 db "Hello, ", 0
2Len equ $ - MSG2

section .bss
name resb 16

section .text
call printMsg
call readName
call printMsg2
call printName

mov eax, 1
int 0x80

printMsg:
  print MSG1, 1Len
 readName:
  read name, 16
 printMsg2:
  print MSG2, 2Len
 printName:
  print name, 16
